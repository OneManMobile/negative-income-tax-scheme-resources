***NYE TIDER I KARTOFFELBY***

Lad os tage p� bes�g i Kartoffelby. Her er alle helt vilde med kartofler. 
De vil allesammen gerne lave s� mange kartofler s� muligt, som kan spises eller s�lges til gennemrejsende, fra hvem de ogs� kan k�be andet fra end kun kartofler. Hver person i Kartoffelby har dog brug for 1000 kartofler til sig selv om m�neden for at overleve.

Der bor kun 10 personer i Kartoffelby, og siden kartofler er byens varem�rke, er det ogs� det eneste der laves i byen. De er dog ikke allesammen lige gode til det. Faktisk ser det s�dan her ud:
Der er 1 person der ikke kan producere noget, men som stadig har brug for 1000 kartofler om m�neden. 

Der er 1 person, som kan producerer 500 kartofler om m�neden, men derved stadig mangler 500 kartofler, for at klare sig selv. Der er 3 personer som hver kan producerer 1000 kartofler om m�neden, alts� lige nok til sig selv. Der er 3 personer der hver kan producerer 5000 kartofler om m�neden, og der er til sidst 2 mesterh�stere, der hver kan producerer 10.000 kartofler om m�neden.

Hver kartoffel koster altid 1 krone i byen. Skatteprocenten i Kartoffelby er p� 50%, men alle har ogs� et automatisk fradrag p� 1000 kroner fra skatten. Dette vil sige at det bel�b, som hver person skal betale til f�lleskassen hver m�ned, kan skrives som:

Skattebel�b = (Kartoffelindkomst * 0.5) - 1000 kroner

Da man jo godt kan se i Kartoffelby, at ikke alle laver kartofler nok til sig selv for at overleve, har man heldigvis opfundet et system. Alle personer er berettiget til 1000 kroner fra f�lleskassen i KartoffelHj�lp, hvis de ikke selv kan producere mindst 1000 kartofler om m�neden. Hver kartoffel de s�lger, op til de 1000, vil dog fratr�kkes i deres KartoffelHj�lp, da man jo ikke har brug for at hj�lpe folk der kan klare sig selv . Det betyder selvf�lgelig ogs� at der ikke er nogen finansiel grund til at lave kartofler, medmindre man absolut ved man kan lave mere end 1000 kartofler om m�neden, men der er heller ingen der sulter i Kartoffelby. Her er trygt og rart nok.

Byens regnskab ser s�dan her ud hver m�ned:
1 person, der har brug for 1000 kartofler, men ikke selv kan producere nogle: F�r 1000 kroner
1 person, der kan producere 500 kartofler, laver 0 kartofler: F�r 1000 kroner
3 personer, der kan producere 1000 kartofler hver, laver 0 kartofler: F�r 1000 kroner hver
3 personer, der producer 5000 kartofler hver: F�r 3500 kroner hver (efter skat)
2 personer, der producer 10000 kartofler hver: F�r 6000 kroner hver (efter skat)
Kartofler ved m�nedens udgang: (-1000 * 5 + 4000 * 3 + 9000 * 2) = 25.000 kartofler
F�lleskassen ender med: (-1000 * 5 + 1500 * 3 + 4000 * 2) = 7.500 kroner
Alle personer f�r tilsammen efter skat: (1000 * 5 + 3500*3+6000*2) = 27.500 kroner
Byens samlede samfundsoverskud: (-1000 * 5 + 5000 * 3 + 10000 * 2) = 30.000 kroner
Produktionskraft pr krone: 25000 kartofler / (7500+ 27500) kroner i samfundet = 0.71
kapitaljusteret samfundsoverskud: 0.71 produktionskraft * 30.000 samfundsoverskudskroner = 21300 kapitaljusteret samfundsoverskud
K�bekraft pr krone: 21300 kapitaljusteret samfundsoverskud / (7500+ 27500) kroner i samfundet = 0.60 K�bekraft pr krone

----

"MEN HOV!!" Er der en rejsende mand ved navn Milton der siger, da han kommer til byen. "Hvad hvis I bare droppede jeres KartoffelHj�lp, og i stedet brugte jeres skattebel�b fra ligningen SELVOM resultatet ikke er et positivt tal. S� I alts� udbetaler bel�bet fra f�lleskassen til personen, hvis skattebel�bet er et negativt tal, og folk betaler bel�bet til f�lleskassen fra deres indkomst, hvis skattebel�bet er et positivt tal. S� ville folk ogs� kunne tjene penge, uanset hvor mange kartofler de producerede, og alle ville have finansiel grund til at bidrage til systemet."

"Hmmm" siger de allesammen. "Det ville jo betyde at byens nye regnskab ville se s�dan her ud":

1 personer, der har brug for 1000 kartofler, og producerer 0 kartofler: F�r 1000 kroner
1 person, der producerer 500 kartofler: F�r 1250 kroner
3 personer, der producerer 1000 kartofler hver: F�r 1500 kroner hver
3 personer, der producerer 5000 kartofler hver: F�r 3500 kroner hver
2 personer, der producerer 10000 kartofler hver: F�r 6000 kroner hver
Kartofler ved m�nedens udgang: (-1000 * 1 + -500*1 + 0 * 3 + 4000 * 3 + 9000 * 2) = 28.500 kartofler
F�lleskassen f�r: (-1000 * 1 + -750 * 1 + -500 * 3 + 1500 * 3 + 4000 * 2) = 9.250 kroner
Alle personer f�r tilsammen udbetalt: (1000 * 1 + 1250 * 1 + 1500 * 3 + 3500*3+6000*2) = 29.250 kroner
Byens samlede samfundsoverskud: (-1000 * 1 + -250*1 + 500* 3 + 5000 *3 + 10000 * 2) = 35.250 kroner

RIGDOMSKAGEN:

Produktionskraft pr krone: 38500 kartofler / (9250+ 29250) kroner i samfundet = 0.74
kapitaljusteret samfundsoverskud: 0,74 produktionskraft * 31750 samfundsoverskudskroner = 23495 kapitaljusteret samfundsoverskud

(produktionskraft per krone gange samlet overskud i samfundet = justeret samfundsoverskud)

RIGDOMSKAGE PER KRONE:

K�bekraft pr krone: 23495 kapitaljusteret samfundsoverskud / (9250+ 29250) kroner i samfundet = 0.61 K�bekraft pr krone

-----
Resultat:
Alle indbyggerne i Kartoffelby blev fattigere hver m�ned, end de ville v�re blevet med Borgerl�n gennem Negativ Indkomstskat. Ingen af indbyggerne blev rigere af det gamle system, hverken i produktionmuligheder eller kontant.
Antallet af kartofler der blev produceret i Kartoffelby var 3500 lavere om m�neden end det beh�vede at v�re. (12% f�rre kartofler)
Det produktive samfundsoverskud var 9,8% mindre om m�neden i hele byen, end det beh�vede at v�re. (17857 / 19802)
P� 1 �r, kunne Kartoffelby have f�et:
42.000 flere kartofler (1,68 m�neders ekstra kartofler)
23.340 produktionsenheder i samfundsoverskud (tilsvarer 2,3 m�neder af byens eget kartoffelforbrug gratis, ved denne ekstra rigdom)
21.000 kroner mere til indbyggerne (2,1 gange af en mesterh�sters overskud fordelt ud)
21.000 kroner mere i f�lleskassen (2,8 m�neders ekstra indt�gt i forhold til det gamle system)
P� 10 �r, kunne Kartoffelby have f�et:
420.000 ekstra kartofler (3,5 �rs gratis h�st fra en mesterh�ster)
233.400 produktionsenheder i samfundsoverskud (tilsvarer 1,9 �r af byens eget kartoffelforbrug gratis, ved denne ekstra rigdom)
210.000 kroner mere til indbyggerne (1,75 �rs h�stv�rdi fra en mesterh�ster, fordelt ud)
210.000 kroner mere i f�lleskassen (2,3 �rs ekstra indkomst)
-----
Hvorn�r g�r vi gang med at g�re tingene bedre i Kartoffelby?